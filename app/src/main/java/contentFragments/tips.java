package contentFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.pripadp.priezpourlesamedupurgatoire111.R;

import appHelpers.FontSizeHelper;

public class tips extends Fragment
{
    public static tips newInstance ()
    {
        return (new tips());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View result=inflater.inflate(R.layout.fragment_tips, container, false);
        TextView tips_Contents_1 = result.findViewById(R.id.tips_content_1);
        TextView tips_Contents_2 = result.findViewById(R.id.tips_content_2);
        FontSizeHelper.sync__fs(getContext(), tips_Contents_1);
        FontSizeHelper.sync__fs(getContext(), tips_Contents_2);
        return result;
    }

}

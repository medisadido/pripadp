package contentFragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.pripadp.priezpourlesamedupurgatoire111.PrayOfPrayContent;
import com.pripadp.priezpourlesamedupurgatoire111.R;

public class prays extends Fragment
{
    private Button[] mPrays;
    public static prays newInstance ()
    {
        return (new prays());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View result=inflater.inflate(R.layout.fragment_prays, container, false);
        mPrays = new Button[8];
        mPrays[0] = result.findViewById(R.id.pray_pray_1);
        mPrays[1] = result.findViewById(R.id.pray_pray_2);
        mPrays[2] = result.findViewById(R.id.pray_pray_3);
        mPrays[3] = result.findViewById(R.id.pray_pray_4);
        mPrays[4] = result.findViewById(R.id.pray_pray_5);
        mPrays[5] = result.findViewById(R.id.pray_pray_6);
        mPrays[6] = result.findViewById(R.id.pray_pray_7);
        mPrays[7] = result.findViewById(R.id.pray_pray_8);

        listenButtons();
        // Set onClickListener to buttons
        result.findViewById(R.id.intro_title);
        return result;
    }

    private void listenButtons() {
        for(Button btn:mPrays)
        {
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int num =Integer.parseInt(String.valueOf ( ((Button)v).getText().charAt(0)))  ;
                    Intent intent = new Intent(getContext(), PrayOfPrayContent.class);
                    intent.putExtra("num",num);
                    startActivity(intent);
                }
            });
        }

    }

}

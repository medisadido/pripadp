package contentFragments;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.pripadp.priezpourlesamedupurgatoire111.R;

import MyViews.PrayMomentTextView;
import MyViews.SimpleContentTextView;
import MyViews.TitleTextView;
import appHelpers.FontSizeHelper;
public class intro extends Fragment {
    LinearLayout mLinearLayout;
    public static intro newInstance ()
    {
        return (new intro());
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout of MainFragment
        View result=inflater.inflate(R.layout.fragment_intro, container, false);
        result.findViewById(R.id.intro_title);
        result.findViewById(R.id.intro_subtitle);
        mLinearLayout = result.findViewById(R.id.intro_content_linear);
        String[] parts = getContext().getResources().getString(R.string.pray_moments).split("§§§");

        for (int i = 0; i < parts.length; i++) {
            if (!parts[i].trim().equals("") && i < parts.length-2) {
                PrayMomentTextView someText = new PrayMomentTextView(getContext());
                someText.setText(parts[i]);
                FontSizeHelper.sync__fs(getContext(), someText);
                mLinearLayout.addView(someText);
            }
            if (i==parts.length - 1)
            {
                TitleTextView someText = new TitleTextView(getContext());
                someText.setText(parts[i]);
                FontSizeHelper.sync__fs(getContext(), someText);
                mLinearLayout.addView(someText);
            }
        }
        TextView intro_text1 = result.findViewById(R.id.intro_text1);
        TextView intro_text2 = result.findViewById(R.id.intro_text2);
        TextView intro_text3 = result.findViewById(R.id.intro_text3);
        TextView intro_text4 = result.findViewById(R.id.intro_text4);
        TextView intro_text5 = result.findViewById(R.id.intro_text5);
        FontSizeHelper.sync__fs(getContext(), intro_text1);
        FontSizeHelper.sync__fs(getContext(), intro_text2);
        FontSizeHelper.sync__fs(getContext(), intro_text3);
        FontSizeHelper.sync__fs(getContext(), intro_text4);
        FontSizeHelper.sync__fs(getContext(), intro_text5);
        return result;
    }
}
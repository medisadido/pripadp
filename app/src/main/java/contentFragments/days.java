package contentFragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pripadp.priezpourlesamedupurgatoire111.Adapters.NumberAdapter;
import com.pripadp.priezpourlesamedupurgatoire111.NumberDays;
import com.pripadp.priezpourlesamedupurgatoire111.R;

import java.util.ArrayList;

public class days extends Fragment
{
    private RecyclerView mRecyclerView;
    private ArrayList<NumberDays> mNumberData;
    private NumberAdapter mAdapter;
    public static boolean tap_from_adapter =false;

    public static days newInstance ()
    {
        return (new days());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View result=inflater.inflate(R.layout.fragment_days, container, false);
        mRecyclerView = result.findViewById(R.id.days_recyclerView);
        mNumberData = new ArrayList<>();
        return result;
    }

    private void initializeData() {
        for (int i = 1; i < 31; i++) {
            mNumberData.add(new NumberDays(i));
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Context ActivityContext = getContext();

        int gridColumnCount = getResources().getInteger(R.integer.grid_column_count);
        mRecyclerView.setLayoutManager(new GridLayoutManager(ActivityContext,gridColumnCount));

        mAdapter = new NumberAdapter(ActivityContext, mNumberData);
        mRecyclerView.setAdapter(mAdapter);

        initializeData();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(tap_from_adapter)
            this.getActivity().getSupportFragmentManager().beginTransaction()
                    .detach(this)
                    .attach(days.this)
                    .commit();
        tap_from_adapter=false;
    }
}

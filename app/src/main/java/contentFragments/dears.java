package contentFragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pripadp.priezpourlesamedupurgatoire111.Adapters.DearAdapter;
import com.pripadp.priezpourlesamedupurgatoire111.AddDearActivity;
import com.pripadp.priezpourlesamedupurgatoire111.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static android.app.Activity.RESULT_OK;
import static com.pripadp.priezpourlesamedupurgatoire111.MainActivity.mPreferences;

public class dears extends Fragment {
    private static final String ACTIVES_LINKS_KEY = "saved_dear";
    private static final int ADD_DEAR_REQUEST = 3;
    public static DearAdapter mDearAdapter;
    public static List<String> mSavedDear;
    FloatingActionButton fab;

    public static dears newInstance() {
        return (new dears());
    }

    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.fragment_dears, container, false);
        //mPreferences = mContext.get().getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        mPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        mSavedDear = new ArrayList<>();

        System.out.println(mSavedDear);

        Set<String> tmpSet = mPreferences.getStringSet(ACTIVES_LINKS_KEY, new HashSet<>(Collections.singletonList("")));
        if (tmpSet != null) {
            mSavedDear.addAll(tmpSet);
        }
        if (mSavedDear.size() > 1 && mSavedDear.get(0).equals("")) {
            mSavedDear.remove(0);
        }

        listView = result.findViewById(R.id.dears_list);
        fab = result.findViewById(R.id.fab);
        return result;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDearAdapter = new DearAdapter(getActivity(), mSavedDear);
        listView.setAdapter(mDearAdapter);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AddDearActivity.class);
                startActivityForResult(intent, ADD_DEAR_REQUEST);
            }
        });
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        mDearAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_DEAR_REQUEST && resultCode == RESULT_OK) {
            System.out.println(mSavedDear.size());
            mDearAdapter = new DearAdapter(getActivity(), mSavedDear);
            listView.setAdapter(mDearAdapter);
            //mDearAdapter.notifyDataSetChanged();
        }
    }
}

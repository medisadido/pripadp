package MyViews;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;

import com.pripadp.priezpourlesamedupurgatoire111.R;

public class PrayMomentTextView extends androidx.appcompat.widget.AppCompatTextView {
    public PrayMomentTextView(@NonNull Context context) {
        super(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.setMargins(15, 1, 10, 10);
        setPadding(30, 1, 10, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            setTextAlignment(TEXT_ALIGNMENT_CENTER);
        }
        setGravity(Gravity.CENTER);
        this.setTextSize(getResources().getDimension(R.dimen.defaultTextSize));
        setTextColor(context.getResources().getColor(R.color.blue_grey));
        this.setTypeface(this.getTypeface(), Typeface.BOLD);
        this.setLayoutParams(params);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            this.setBackground(AppCompatResources.getDrawable(context, R.drawable.pray_moment_border));
        } else {
            this.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
        this.setPadding(8, 8, 8, 8);


    }
}

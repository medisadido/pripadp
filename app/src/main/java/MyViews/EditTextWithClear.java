package MyViews;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.res.ResourcesCompat;

import com.pripadp.priezpourlesamedupurgatoire111.R;

public class EditTextWithClear extends AppCompatEditText {
    Drawable mClearButtonImage;

    public EditTextWithClear(@NonNull Context context) {
        super(context);
        init();

    }

    public EditTextWithClear(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public EditTextWithClear(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mClearButtonImage = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_baseline_clear_24, null);
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                System.out.println(event.getAction());
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        System.out.println("down");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            if ((getCompoundDrawablesRelative()[2] != null)) {
                                float clearButtonStart; // Used for LTR languages
                                float clearButtonEnd;  // Used for RTL languages
                                boolean isClearButtonClicked = false;

                                // Detect the touch in RTL or LTR layout direction.
                                if (getLayoutDirection() == LAYOUT_DIRECTION_RTL) {
                                    // If RTL, get the end of the button on the left side.
                                    clearButtonEnd = mClearButtonImage.getIntrinsicWidth() + getPaddingStart();
                                    // If the touch occurred before the end of the button, set isClearButtonClicked to true.
                                    if (event.getX() < clearButtonEnd) {
                                        isClearButtonClicked = true;
                                    }
                                } else {
                                    // Layout is LTR.
                                    // Get the start of the button on the right side.
                                    clearButtonStart = (getWidth() - getPaddingEnd() - mClearButtonImage.getIntrinsicWidth());
                                    // If the touch occurred after the start of the button,set isClearButtonClicked to true.
                                    if (event.getX() > clearButtonStart) {
                                        isClearButtonClicked = true;
                                    }
                                }

                                // Check for actions if the button is tapped.
                                if (isClearButtonClicked) {
                                    // Check for ACTION_DOWN (always occurs before ACTION_UP).
                                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                                        // Switch to the black version of clear button.
                                        mClearButtonImage = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_baseline_clear_24, null);
                                        showClearButton();
                                    }
                                    if (event.getAction() == MotionEvent.ACTION_UP) {
                                        mClearButtonImage = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_baseline_clear_opaque_24, null);
                                        if (getText() != null) {
                                            getText().clear();
                                        }
                                        hideClearButton();
                                        return true;
                                    }
                                } else {
                                    return false;
                                }
                            }
                        }
                        return false;
                    case MotionEvent.ACTION_UP:
                        System.out.println("up");
                        v.performClick();
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showClearButton();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * Shows the clear (X) button.
     */
    private void showClearButton() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, mClearButtonImage, null);
        }
    }

    private void hideClearButton() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null);
        }
    }
}

package com.pripadp.priezpourlesamedupurgatoire111;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreferenceCompat;

import java.util.Calendar;

import static appHelpers.DataUtils.lang_of_lang;
import static appHelpers.DataUtils.nav_of_nav;
import static appHelpers.DataUtils.rap_of_rap;

public class SettingsActivity extends AppCompatActivity {
    public static final String KEY_PREF_MODE = "mode_preference";
    public static final String KEY_PREF_POLICE = "police_preference";
    public static final String KEY_PREF_LANGUAGE = "language_preference";
    public static final String KEY_PREF_NAV_VISIBILITY = "visibility_preference";
    public static final String KEY_PREF_LOCATION_CHOICE = "location_choice_preference";

    private static NotificationManager mNotificationManager;
    private static final int NOTIFICATION_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
            ListPreference themeListPreference = findPreference(SettingsActivity.KEY_PREF_MODE);
            ListPreference policeListPreference = findPreference(SettingsActivity.KEY_PREF_POLICE);
            final Preference locationChooseListPreference = findPreference(SettingsActivity.KEY_PREF_LOCATION_CHOICE);
            locationChooseListPreference.setEnabled(nav_of_nav);

            ListPreference languageListPreference = findPreference(SettingsActivity.KEY_PREF_LANGUAGE);
            if (languageListPreference != null) {
                languageListPreference.setDefaultValue(lang_of_lang);
            }
            final SwitchPreferenceCompat navVisibillity = findPreference(SettingsActivity.KEY_PREF_NAV_VISIBILITY);
            if (navVisibillity != null) {
                navVisibillity.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        nav_of_nav = !nav_of_nav;
                        navVisibillity.setChecked(nav_of_nav);
                        mNotificationManager = (NotificationManager) requireActivity().getSystemService(NOTIFICATION_SERVICE);
                        Intent notifyIntent = new Intent(requireActivity(), AlarmReceiver.class);
                        final PendingIntent notifyPendingIntent = PendingIntent.getBroadcast(requireActivity(), NOTIFICATION_ID, notifyIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );
                        final AlarmManager alarmManager = (AlarmManager) requireActivity().getSystemService(ALARM_SERVICE);

                        String toastMessage;
                        if (nav_of_nav) {
                            locationChooseListPreference.setEnabled(true);

                            Calendar calendar = Calendar.getInstance();
                            calendar.setTimeInMillis(System.currentTimeMillis());
                            String[] tipa = rap_of_rap.split(":");
                            int hour = Integer.parseInt(tipa[0]);
                            int min = Integer.parseInt(tipa[1]);

                            calendar.set(Calendar.HOUR_OF_DAY, hour);
                            calendar.set(Calendar.MINUTE, min);

                            if (alarmManager != null) {
                                alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                                        calendar.getTimeInMillis(),
                                        AlarmManager.INTERVAL_DAY,
                                        notifyPendingIntent
                                );
                            }
                            toastMessage = requireActivity().getResources().getString(R.string.next_remin_in) + hour + " H " +
                                    min + " min";
                        } else {
                            locationChooseListPreference.setEnabled(false);
                            mNotificationManager.cancelAll();
                            if (alarmManager != null) {
                                alarmManager.cancel(notifyPendingIntent);
                            }
                            toastMessage = requireActivity().getResources().getString(R.string.cancel_reminder);
                        }
                        Toast.makeText(requireActivity(), toastMessage, Toast.LENGTH_SHORT).show();
                        return nav_of_nav;
                    }
                });
                if (locationChooseListPreference != null) {
                    locationChooseListPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                        @Override
                        public boolean onPreferenceClick(Preference preference) {
                            DialogFragment newFragment = new TimePickerFragment();
                            newFragment.show(requireActivity().getSupportFragmentManager(), "timePicker");
                            return false;
                        }
                    });
                }
            }
        }

    }

    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            rap_of_rap = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);

            mNotificationManager = (NotificationManager) requireActivity().getSystemService(NOTIFICATION_SERVICE);
            Intent notifyIntent = new Intent(requireActivity(), AlarmReceiver.class);
            final PendingIntent notifyPendingIntent = PendingIntent.getBroadcast(requireActivity(), NOTIFICATION_ID, notifyIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
            final AlarmManager alarmManager = (AlarmManager) requireActivity().getSystemService(ALARM_SERVICE);


            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            String[] tipa = rap_of_rap.split(":");
            int hour = Integer.parseInt(tipa[0]);
            int min = Integer.parseInt(tipa[1]);

            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, min);

            if (alarmManager != null) {
                alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                        calendar.getTimeInMillis(),
                        AlarmManager.INTERVAL_DAY,
                        notifyPendingIntent
                );
            }
            String toastMessage = requireActivity().getResources().getString(R.string.next_remin_in) + hour + " H " +
                    min + " min";
            Toast.makeText(requireActivity(), toastMessage, Toast.LENGTH_SHORT).show();
        }
    }

    public void OnApplyClicked(View view) {
        setResult(RESULT_OK);
        finish();
    }
}
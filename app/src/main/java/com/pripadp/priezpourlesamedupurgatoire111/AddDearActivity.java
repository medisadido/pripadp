package com.pripadp.priezpourlesamedupurgatoire111;

import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.pripadp.priezpourlesamedupurgatoire111.Adapters.DearAdapter;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static com.pripadp.priezpourlesamedupurgatoire111.MainActivity.mPreferences;
import static contentFragments.dears.mSavedDear;
import static contentFragments.dears.mDearAdapter;

public class AddDearActivity extends AppCompatActivity {
    EditText mNameDear;
    Spinner mLinkDear;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_dear);
        mNameDear = findViewById(R.id.name_dear);
        mLinkDear = findViewById(R.id.link_dear);
        ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray((R.array.family_link_array)));
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mLinkDear.setAdapter(aa);
        mLinkDear.setSelection(15,true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
    }
    public void OnApplyClicked(View view) {
        String enteredName = mNameDear.getText().toString();
        String enteredLink = mLinkDear.getSelectedItem().toString();
        String toregister = enteredName + "£°£" + enteredLink;
        if (!mSavedDear.contains(toregister) && !enteredName.equals("")) {
            mSavedDear.add(toregister);
        }
        SharedPreferences.Editor prefsEditor = mPreferences.edit();
        Set<String> mySet = new HashSet<>();
        for (String x : mSavedDear)
            mySet.add((String) x);
        prefsEditor.putStringSet("saved_dear", mySet);
        prefsEditor.apply();
        mDearAdapter.notifyDataSetChanged();
        setResult(RESULT_OK);
        finish();
    }
}
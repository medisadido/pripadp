package com.pripadp.priezpourlesamedupurgatoire111;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.text.HtmlCompat;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import MyViews.SimpleContentTextView;
import MyViews.TitleTextView;
import appHelpers.FontSizeHelper;

public class PrayOfDaysContent extends AppCompatActivity {
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    String mTitle, mContent, mPray;
    LinearLayout mLinearLayout;
    ImageView shareImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pray_of_day);
        shareImage = findViewById(R.id.share_prayOfDay);
        mLinearLayout = findViewById(R.id.prayofDayContentContainer);
        TextView praySubtitle = findViewById(R.id.days_pray_subtitle);
        mCollapsingToolbarLayout = findViewById(R.id.toolbar_layout);
        String[] contentInfo = getResources().getStringArray((R.array.days_pray_content));
        String[] subInfo = getResources().getStringArray((R.array.days_pray_subs));
        String[] prayInfo = getResources().getStringArray((R.array.days_pray_pray));
        int daysNumber = (getIntent().getIntExtra("number", 0) - 1);
        mTitle = subInfo[daysNumber];
        mPray = prayInfo[daysNumber];
        mContent = contentInfo[daysNumber];

        String[] parts = mContent.split("§§§");

        for (int i = 0; i < parts.length; i++) {
            if (!parts[i].trim().equals("")) {
                SimpleContentTextView someText = new SimpleContentTextView(this);
                if (i == parts.length - 1) {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.setMargins(16, 1, 10, 50);
                    someText.setLayoutParams(params);
                }
                if (i % 2 != 0) {
                    someText = new TitleTextView(this);
                }
                someText.setText(parts[i]);
                FontSizeHelper.sync__fs(this, someText);
                mLinearLayout.addView(someText);
            }
        }

        TitleTextView someText = new TitleTextView(this);
        someText.setText(mPray);
        FontSizeHelper.sync__fs(this, someText);
        mLinearLayout.addView(someText);


        mCollapsingToolbarLayout.setTitle(mTitle);

        praySubtitle.setText(mTitle);
        /*
        prayContent.setText(mContent);
        pray.setText(mPray);
        prayContent.setTextIsSelectable(true);
         */

        praySubtitle.setTextIsSelectable(true);
        praySubtitle.setTextIsSelectable(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }

    }

    public void sharePrayOfDay(View view) {
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
        shareImage.startAnimation(shake);
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, mTitle + "\n\n" + mContent + "\n\n" + mPray);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.sharing_app_text_intent_title)));
    }

    AttributeSet mAttributeSet = new AttributeSet() {
        @Override
        public int getAttributeCount() {
            return 0;
        }

        @Override
        public String getAttributeName(int index) {
            return null;
        }

        @Override
        public String getAttributeValue(int index) {
            return null;
        }

        @Override
        public String getAttributeValue(String namespace, String name) {
            return null;
        }

        @Override
        public String getPositionDescription() {
            return null;
        }

        @Override
        public int getAttributeNameResource(int index) {
            return 0;
        }

        @Override
        public int getAttributeListValue(String namespace, String attribute, String[] options, int defaultValue) {
            return 0;
        }

        @Override
        public boolean getAttributeBooleanValue(String namespace, String attribute, boolean defaultValue) {
            return false;
        }

        @Override
        public int getAttributeResourceValue(String namespace, String attribute, int defaultValue) {
            return 0;
        }

        @Override
        public int getAttributeIntValue(String namespace, String attribute, int defaultValue) {
            return 0;
        }

        @Override
        public int getAttributeUnsignedIntValue(String namespace, String attribute, int defaultValue) {
            return 0;
        }

        @Override
        public float getAttributeFloatValue(String namespace, String attribute, float defaultValue) {
            return 0;
        }

        @Override
        public int getAttributeListValue(int index, String[] options, int defaultValue) {
            return 0;
        }

        @Override
        public boolean getAttributeBooleanValue(int index, boolean defaultValue) {
            return false;
        }

        @Override
        public int getAttributeResourceValue(int index, int defaultValue) {
            return 0;
        }

        @Override
        public int getAttributeIntValue(int index, int defaultValue) {
            return 0;
        }

        @Override
        public int getAttributeUnsignedIntValue(int index, int defaultValue) {
            return 0;
        }

        @Override
        public float getAttributeFloatValue(int index, float defaultValue) {
            return 0;
        }

        @Override
        public String getIdAttribute() {
            return null;
        }

        @Override
        public String getClassAttribute() {
            return null;
        }

        @Override
        public int getIdAttributeResourceValue(int defaultValue) {
            return 0;
        }

        @Override
        public int getStyleAttribute() {
            return 0;
        }
    };
}

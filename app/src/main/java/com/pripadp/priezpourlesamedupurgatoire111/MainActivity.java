package com.pripadp.priezpourlesamedupurgatoire111;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import com.google.android.material.navigation.NavigationView;

import java.util.Locale;
import java.util.Objects;

import appHelpers.LanguageHelper;
import appHelpers.ThemeHelper;
import contentFragments.days;
import contentFragments.intro;
import contentFragments.prays;
import contentFragments.tips;
import contentFragments.dears;

import static appHelpers.DataUtils.lang_of_lang;
import static appHelpers.DataUtils.rap_of_rap;
import static appHelpers.DataUtils.nav_of_nav;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final String FRAGMENT_KEY = "current_fragment_key";
    private int FRAGMENT_KEY_TAG;

    private static final int SETTING_ACTIVITY_REQUEST_CODE = 3,
            FRAGMENT_INTRO = 0, FRAGMENT_DAYS = 1, FRAGMENT_TIPS = 2,
            FRAGMENT_PRAYS = 3, FRAGMENT_DEARS = 4;

    public static SharedPreferences mPreferences;
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private Fragment introFragment, daysFragment, tipsFragment, praysFragment, dearssFragment;
    private static NotificationManager mNotificationManager;
    private static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        super.onCreate(savedInstanceState);
        settingProcess();
        setContentView(R.layout.activity_main);
        configureToolbar();
        configureDrawerLayout();
        configureNavigationView();
        String sharedPrefFile = "com.pripadpp.priezpourlesamedupurgatoire111";
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        FRAGMENT_KEY_TAG = mPreferences.getInt(FRAGMENT_KEY, 0);
        showFragment_or_Activity(FRAGMENT_KEY_TAG);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Objects.requireNonNull(getSupportActionBar()).setBackgroundDrawable(
                    new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        createNotificationChannel();

    }

    private void configureToolbar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    private void configureDrawerLayout() {
        this.mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                this.mDrawerLayout,
                mToolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void configureNavigationView() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (this.mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        System.out.println(item.getTitle());

        int id = item.getItemId();
        if (id == R.id.nav_intro) {
            showFragment_or_Activity(FRAGMENT_INTRO);
        } else if (id == R.id.nav_days) {
            showFragment_or_Activity(FRAGMENT_DAYS);
        } else if (id == R.id.nav_tips) {
            showFragment_or_Activity(FRAGMENT_TIPS);
        } else if (id == R.id.nav_prays) {
            showFragment_or_Activity(FRAGMENT_PRAYS);
        } else if (id == R.id.nav_dear) {
            showFragment_or_Activity(FRAGMENT_DEARS);
        } else if (id == R.id.nav_setting) {
            Intent Setting_intent = new Intent(this, SettingsActivity.class);
            startActivityForResult(Setting_intent, SETTING_ACTIVITY_REQUEST_CODE);
        } else if (id == R.id.nav_about) {
            Intent About_intent = new Intent(this, AboutActivity.class);
            startActivity(About_intent);
        }
        this.mDrawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    private void showFragment_or_Activity(int fragmentintent) {
        FRAGMENT_KEY_TAG = fragmentintent;
        switch (fragmentintent) {
            case FRAGMENT_INTRO:
                if (this.introFragment == null) {
                    introFragment = intro.newInstance();
                }
                startTransactionFragment(this.introFragment);
                break;

            case FRAGMENT_DAYS:
                if (this.daysFragment == null) {
                    daysFragment = days.newInstance();
                }
                startTransactionFragment(this.daysFragment);
                break;

            case FRAGMENT_TIPS:
                if (this.tipsFragment == null) {
                    tipsFragment = tips.newInstance();
                }
                startTransactionFragment(this.tipsFragment);
                break;

            case FRAGMENT_DEARS:
                if (this.tipsFragment == null) {
                    dearssFragment = dears.newInstance();
                }
                startTransactionFragment(this.dearssFragment);
                break;

            case FRAGMENT_PRAYS:
                if (this.praysFragment == null) {
                    praysFragment = prays.newInstance();
                }
                startTransactionFragment(this.praysFragment);
                break;
            default:
                break;
        }
    }

    private void startTransactionFragment(Fragment fragment) {
        if (fragment != null && !fragment.isVisible()) {
            getSupportFragmentManager().beginTransaction().replace(R.id.main_host, fragment).commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SETTING_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Intent restarintent = new Intent(MainActivity.this, MainActivity.class);
                startActivity(restarintent);
                finish();
            }
        }
    }

    private void settingProcess() {
        PreferenceManager.setDefaultValues(this, R.xml.root_preferences, false);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        String mode_pref = sharedPref.getString(SettingsActivity.KEY_PREF_MODE, "dark");

        String local = Locale.getDefault().getLanguage();

        lang_of_lang = sharedPref.getString(SettingsActivity.KEY_PREF_LANGUAGE,
                (!local.equals(Locale.FRENCH.getLanguage()) && !local.equals(Locale.ENGLISH.getLanguage()))
                        ? Locale.ENGLISH.getLanguage() : local);
        rap_of_rap = sharedPref.getString(SettingsActivity.KEY_PREF_LOCATION_CHOICE, "00:00");
        nav_of_nav = sharedPref.getBoolean(SettingsActivity.KEY_PREF_NAV_VISIBILITY, false);

        LanguageHelper.set__lang(getResources(), lang_of_lang);
        if (mode_pref != null) {
            ThemeHelper.applyTheme(mode_pref);
        }
    }

    private void createNotificationChannel() {
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(PRIMARY_CHANNEL_ID,
                    "pray Notification",
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription("Notifies every seted time to pray ");
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(new Bundle());
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putInt(FRAGMENT_KEY, FRAGMENT_KEY_TAG);
        preferencesEditor.apply();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            Intent Setting_intent = new Intent(this, SettingsActivity.class);
            startActivityForResult(Setting_intent, SETTING_ACTIVITY_REQUEST_CODE);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
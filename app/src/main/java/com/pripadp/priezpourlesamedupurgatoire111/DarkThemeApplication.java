package com.pripadp.priezpourlesamedupurgatoire111;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import appHelpers.ThemeHelper;

public class DarkThemeApplication extends Application {
    public void onCreate() {
        super.onCreate();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String themePref = sharedPreferences.getString("mode_preference", ThemeHelper.DEFAULT_MODE);
        ThemeHelper.applyTheme(themePref);
    }
}

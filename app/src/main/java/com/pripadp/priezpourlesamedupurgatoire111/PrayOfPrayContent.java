package com.pripadp.priezpourlesamedupurgatoire111;

import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import appHelpers.FontSizeHelper;

public class PrayOfPrayContent extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pray_content);
        TextView title = findViewById(R.id.pray_content_title);
        TextView content = findViewById(R.id.pray_content_content);

        String[] prayTitle = getResources().getStringArray((R.array.pray_pray_title));
        String[] prayContent = getResources().getStringArray((R.array.pray_pray_content));

        int prayNumber = (getIntent().getIntExtra("num",0)-1) ;

        title.setText(prayTitle[prayNumber]);
        content.setText(prayContent[prayNumber]);
        FontSizeHelper.sync__fs(this, content);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
    }
}

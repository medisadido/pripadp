package com.pripadp.priezpourlesamedupurgatoire111.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pripadp.priezpourlesamedupurgatoire111.MainActivity;
import com.pripadp.priezpourlesamedupurgatoire111.NumberDays;
import com.pripadp.priezpourlesamedupurgatoire111.PrayOfDaysContent;
import com.pripadp.priezpourlesamedupurgatoire111.R;

import java.util.ArrayList;

import static contentFragments.days.tap_from_adapter;

public class NumberAdapter extends RecyclerView.Adapter<NumberAdapter.ViewHolder> {
    private TextView mNumber;
    private TextView mTitle;
    private ImageView background;

    private final ArrayList<NumberDays> mNumberDaysData;
    private final Context mContext;
    public static final String CURRENT_DAY_KEY = "current_day_key";

    public NumberAdapter(Context context, ArrayList<NumberDays> numberdata) {
        this.mContext = context;
        this.mNumberDaysData = numberdata;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.days_recyclerview_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NumberDays currentNum = mNumberDaysData.get(position);
        String title = mContext.getResources().getStringArray((R.array.days_pray_subs))[currentNum.getNumber() - 1];
        if (title.length() > 50) {
            title = title.substring(0, 49) + "...";
        }
        if (position <= 29) {
            mNumber.setText(String.valueOf(currentNum.getNumber()));
            mTitle.setText(title);
            background.setImageResource(R.drawable.background_fleur);
            mNumber.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fleur_1));
            if (position == (MainActivity.mPreferences.getInt(CURRENT_DAY_KEY, 0) - 1)) {
                mNumber.setTextColor(Color.WHITE);
                mNumber.setBackground(mContext.getResources().getDrawable(R.drawable.ic_fleur_2));
                mTitle.setTextColor(mContext.getResources().getColor(R.color.digit_color));
                mTitle.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
                background.setImageResource(R.drawable.background_fleur_selected);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mNumberDaysData.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mNumber = itemView.findViewById(R.id.days_number);
            mTitle = itemView.findViewById(R.id.days_title);
            background = itemView.findViewById(R.id.bImage);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            tap_from_adapter = true;

            NumberDays currentDays = mNumberDaysData.get(getAdapterPosition());

            SharedPreferences.Editor preferencesEditor = MainActivity.mPreferences.edit();
            preferencesEditor.putInt(CURRENT_DAY_KEY, currentDays.getNumber());
            preferencesEditor.apply();

            Intent detailIntent = new Intent(mContext, PrayOfDaysContent.class);
            detailIntent.putExtra("number", currentDays.getNumber());

            mContext.startActivity(detailIntent);
        }
    }
}

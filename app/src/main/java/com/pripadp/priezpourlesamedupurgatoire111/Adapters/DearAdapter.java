package com.pripadp.priezpourlesamedupurgatoire111.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.pripadp.priezpourlesamedupurgatoire111.R;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.pripadp.priezpourlesamedupurgatoire111.MainActivity.mPreferences;
import static contentFragments.dears.mDearAdapter;
import static contentFragments.dears.mSavedDear;

public class DearAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final List<String> items;
    public DearAdapter(Activity context, List<String> items) {
        super(context, R.layout.dear_item, items);
        this.context = context;
        this.items = items;
    }
    static class ViewHolder {
        private TextView itemName;
        private TextView itemLink;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder mViewHolder = null;
        if (convertView == null) {
            mViewHolder = new ViewHolder();
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.dear_item, parent, false);
            mViewHolder.itemName = convertView.findViewById(R.id.name_item);
            mViewHolder.itemLink = convertView.findViewById(R.id.link_item);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        final String currenString = items.get(position);
        if (items.size() == 1 && currenString.equals("")) {
            mViewHolder.itemName.setText(context.getResources().getString(R.string.no_saved_dear));
            mViewHolder.itemLink.setText(context.getResources().getString(R.string.click_on_below_button));
        } else {
            if (!currenString.equals("")) {
                mViewHolder.itemName.setText(currenString.substring(0, currenString.indexOf("£°£")));
                mViewHolder.itemLink.setText(currenString.substring(currenString.indexOf("£°£") + 3));
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        String[] actions = {context.getResources().getString(R.string.suppress),context.getResources().getString(R.string.modify), context.getResources().getString(R.string.add_favorite)};
                        builder.setItems(actions, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                        builder.setTitle("suppression")
                                                .setMessage(context.getResources().getString(R.string.confirm_suppress) + " ?")
                                                .setPositiveButton(context.getResources().getString(R.string.suppress), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        mSavedDear.remove(currenString);
                                                        SharedPreferences.Editor prefsEditor = mPreferences.edit();
                                                        Set<String> mySet = new HashSet<>();
                                                        for (String x : mSavedDear)
                                                            mySet.add((String) x);
                                                        prefsEditor.putStringSet("saved_dear", mySet);
                                                        prefsEditor.apply();
                                                        mDearAdapter.notifyDataSetChanged();
                                                    }
                                                })
                                                .setNeutralButton(context.getResources().getString(R.string.cancel), null)
                                                .setNegativeButton("Cancel", null);
                                        builder.create().show();
                                        break;
                                    case 1:
                                        break;
                                }
                            }
                        });
                        builder.create().show();
                    }
                });
            }
        }
        return convertView;
    }
}
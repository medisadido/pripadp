package com.pripadp.priezpourlesamedupurgatoire111;

import android.os.Parcel;
import android.os.Parcelable;

public class NumberDays implements Parcelable {

        private final int number;

    public NumberDays(int titlo) {
            this.number = titlo;
        }

        protected NumberDays(Parcel in) {
            number = in.readInt();
        }

        public static final Parcelable.Creator<NumberDays> CREATOR = new Creator<NumberDays>() {
            @Override
            public NumberDays createFromParcel(Parcel in) {
                return new NumberDays(in);
            }

            @Override
            public NumberDays[] newArray(int size) {
                return new NumberDays[size];
            }
        };

        public int getNumber() {
            return number;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(number);
        }
    }

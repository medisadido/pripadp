package appHelpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.TextView;

import androidx.preference.PreferenceManager;

import com.pripadp.priezpourlesamedupurgatoire111.R;
import com.pripadp.priezpourlesamedupurgatoire111.SettingsActivity;

public class FontSizeHelper {

    public static void sync__fs(Context context, TextView textView) {
        PreferenceManager.setDefaultValues(context, R.xml.root_preferences, false);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        int police_pref = Integer.parseInt(sharedPref.getString(SettingsActivity.KEY_PREF_POLICE, "15"));
        textView.setTextSize(police_pref);
    }
}
